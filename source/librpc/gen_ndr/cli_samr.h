#include "librpc/gen_ndr/ndr_samr.h"
#ifndef __CLI_SAMR__
#define __CLI_SAMR__
NTSTATUS rpccli_samr_Connect(struct rpc_pipe_client *cli,
			     TALLOC_CTX *mem_ctx,
			     uint16_t *system_name,
			     uint32_t access_mask,
			     struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_Close(struct rpc_pipe_client *cli,
			   TALLOC_CTX *mem_ctx,
			   struct policy_handle *handle);
NTSTATUS rpccli_samr_SetSecurity(struct rpc_pipe_client *cli,
				 TALLOC_CTX *mem_ctx,
				 struct policy_handle *handle,
				 uint32_t sec_info,
				 struct sec_desc_buf *sdbuf);
NTSTATUS rpccli_samr_QuerySecurity(struct rpc_pipe_client *cli,
				   TALLOC_CTX *mem_ctx,
				   struct policy_handle *handle,
				   uint32_t sec_info,
				   struct sec_desc_buf **sdbuf);
NTSTATUS rpccli_samr_Shutdown(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_LookupDomain(struct rpc_pipe_client *cli,
				  TALLOC_CTX *mem_ctx,
				  struct policy_handle *connect_handle,
				  struct lsa_String *domain_name,
				  struct dom_sid2 **sid);
NTSTATUS rpccli_samr_EnumDomains(struct rpc_pipe_client *cli,
				 TALLOC_CTX *mem_ctx,
				 struct policy_handle *connect_handle,
				 uint32_t *resume_handle,
				 struct samr_SamArray **sam,
				 uint32_t buf_size,
				 uint32_t *num_entries);
NTSTATUS rpccli_samr_OpenDomain(struct rpc_pipe_client *cli,
				TALLOC_CTX *mem_ctx,
				struct policy_handle *connect_handle,
				uint32_t access_mask,
				struct dom_sid2 *sid,
				struct policy_handle *domain_handle);
NTSTATUS rpccli_samr_QueryDomainInfo(struct rpc_pipe_client *cli,
				     TALLOC_CTX *mem_ctx,
				     struct policy_handle *domain_handle,
				     uint16_t level,
				     union samr_DomainInfo **info);
NTSTATUS rpccli_samr_SetDomainInfo(struct rpc_pipe_client *cli,
				   TALLOC_CTX *mem_ctx,
				   struct policy_handle *domain_handle,
				   uint16_t level,
				   union samr_DomainInfo *info);
NTSTATUS rpccli_samr_CreateDomainGroup(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *domain_handle,
				       struct lsa_String *name,
				       uint32_t access_mask,
				       struct policy_handle *group_handle,
				       uint32_t *rid);
NTSTATUS rpccli_samr_EnumDomainGroups(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      struct policy_handle *domain_handle,
				      uint32_t *resume_handle,
				      struct samr_SamArray **sam,
				      uint32_t max_size,
				      uint32_t *num_entries);
NTSTATUS rpccli_samr_CreateUser(struct rpc_pipe_client *cli,
				TALLOC_CTX *mem_ctx,
				struct policy_handle *domain_handle,
				struct lsa_String *account_name,
				uint32_t access_mask,
				struct policy_handle *user_handle,
				uint32_t *rid);
NTSTATUS rpccli_samr_EnumDomainUsers(struct rpc_pipe_client *cli,
				     TALLOC_CTX *mem_ctx,
				     struct policy_handle *domain_handle,
				     uint32_t *resume_handle,
				     uint32_t acct_flags,
				     struct samr_SamArray **sam,
				     uint32_t max_size,
				     uint32_t *num_entries);
NTSTATUS rpccli_samr_CreateDomAlias(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *domain_handle,
				    struct lsa_String *alias_name,
				    uint32_t access_mask,
				    struct policy_handle *alias_handle,
				    uint32_t *rid);
NTSTATUS rpccli_samr_EnumDomainAliases(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *domain_handle,
				       uint32_t *resume_handle,
				       struct samr_SamArray **sam,
				       uint32_t max_size,
				       uint32_t *num_entries);
NTSTATUS rpccli_samr_GetAliasMembership(struct rpc_pipe_client *cli,
					TALLOC_CTX *mem_ctx,
					struct policy_handle *domain_handle,
					struct lsa_SidArray *sids,
					struct samr_Ids *rids);
NTSTATUS rpccli_samr_LookupNames(struct rpc_pipe_client *cli,
				 TALLOC_CTX *mem_ctx,
				 struct policy_handle *domain_handle,
				 uint32_t num_names,
				 struct lsa_String *names,
				 struct samr_Ids *rids,
				 struct samr_Ids *types);
NTSTATUS rpccli_samr_LookupRids(struct rpc_pipe_client *cli,
				TALLOC_CTX *mem_ctx,
				struct policy_handle *domain_handle,
				uint32_t num_rids,
				uint32_t *rids,
				struct lsa_Strings *names,
				struct samr_Ids *types);
NTSTATUS rpccli_samr_OpenGroup(struct rpc_pipe_client *cli,
			       TALLOC_CTX *mem_ctx,
			       struct policy_handle *domain_handle,
			       uint32_t access_mask,
			       uint32_t rid,
			       struct policy_handle *group_handle);
NTSTATUS rpccli_samr_QueryGroupInfo(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *group_handle,
				    enum samr_GroupInfoEnum level,
				    union samr_GroupInfo **info);
NTSTATUS rpccli_samr_SetGroupInfo(struct rpc_pipe_client *cli,
				  TALLOC_CTX *mem_ctx,
				  struct policy_handle *group_handle,
				  enum samr_GroupInfoEnum level,
				  union samr_GroupInfo *info);
NTSTATUS rpccli_samr_AddGroupMember(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *group_handle,
				    uint32_t rid,
				    uint32_t flags);
NTSTATUS rpccli_samr_DeleteDomainGroup(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *group_handle);
NTSTATUS rpccli_samr_DeleteGroupMember(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *group_handle,
				       uint32_t rid);
NTSTATUS rpccli_samr_QueryGroupMember(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      struct policy_handle *group_handle,
				      struct samr_RidTypeArray **rids);
NTSTATUS rpccli_samr_SetMemberAttributesOfGroup(struct rpc_pipe_client *cli,
						TALLOC_CTX *mem_ctx,
						struct policy_handle *group_handle,
						uint32_t unknown1,
						uint32_t unknown2);
NTSTATUS rpccli_samr_OpenAlias(struct rpc_pipe_client *cli,
			       TALLOC_CTX *mem_ctx,
			       struct policy_handle *domain_handle,
			       uint32_t access_mask,
			       uint32_t rid,
			       struct policy_handle *alias_handle);
NTSTATUS rpccli_samr_QueryAliasInfo(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *alias_handle,
				    enum samr_AliasInfoEnum level,
				    union samr_AliasInfo **info);
NTSTATUS rpccli_samr_SetAliasInfo(struct rpc_pipe_client *cli,
				  TALLOC_CTX *mem_ctx,
				  struct policy_handle *alias_handle,
				  enum samr_AliasInfoEnum level,
				  union samr_AliasInfo *info);
NTSTATUS rpccli_samr_DeleteDomAlias(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *alias_handle);
NTSTATUS rpccli_samr_AddAliasMember(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *alias_handle,
				    struct dom_sid2 *sid);
NTSTATUS rpccli_samr_DeleteAliasMember(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *alias_handle,
				       struct dom_sid2 *sid);
NTSTATUS rpccli_samr_GetMembersInAlias(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *alias_handle,
				       struct lsa_SidArray *sids);
NTSTATUS rpccli_samr_OpenUser(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      struct policy_handle *domain_handle,
			      uint32_t access_mask,
			      uint32_t rid,
			      struct policy_handle *user_handle);
NTSTATUS rpccli_samr_DeleteUser(struct rpc_pipe_client *cli,
				TALLOC_CTX *mem_ctx,
				struct policy_handle *user_handle);
NTSTATUS rpccli_samr_QueryUserInfo(struct rpc_pipe_client *cli,
				   TALLOC_CTX *mem_ctx,
				   struct policy_handle *user_handle,
				   uint16_t level,
				   union samr_UserInfo **info);
NTSTATUS rpccli_samr_SetUserInfo(struct rpc_pipe_client *cli,
				 TALLOC_CTX *mem_ctx,
				 struct policy_handle *user_handle,
				 uint16_t level,
				 union samr_UserInfo *info);
NTSTATUS rpccli_samr_ChangePasswordUser(struct rpc_pipe_client *cli,
					TALLOC_CTX *mem_ctx,
					struct policy_handle *user_handle,
					uint8_t lm_present,
					struct samr_Password *old_lm_crypted,
					struct samr_Password *new_lm_crypted,
					uint8_t nt_present,
					struct samr_Password *old_nt_crypted,
					struct samr_Password *new_nt_crypted,
					uint8_t cross1_present,
					struct samr_Password *nt_cross,
					uint8_t cross2_present,
					struct samr_Password *lm_cross);
NTSTATUS rpccli_samr_GetGroupsForUser(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      struct policy_handle *user_handle,
				      struct samr_RidWithAttributeArray **rids);
NTSTATUS rpccli_samr_QueryDisplayInfo(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      struct policy_handle *domain_handle,
				      uint16_t level,
				      uint32_t start_idx,
				      uint32_t max_entries,
				      uint32_t buf_size,
				      uint32_t *total_size,
				      uint32_t *returned_size,
				      union samr_DispInfo *info);
NTSTATUS rpccli_samr_GetDisplayEnumerationIndex(struct rpc_pipe_client *cli,
						TALLOC_CTX *mem_ctx,
						struct policy_handle *domain_handle,
						uint16_t level,
						struct lsa_String name,
						uint32_t *idx);
NTSTATUS rpccli_samr_TestPrivateFunctionsDomain(struct rpc_pipe_client *cli,
						TALLOC_CTX *mem_ctx,
						struct policy_handle *domain_handle);
NTSTATUS rpccli_samr_TestPrivateFunctionsUser(struct rpc_pipe_client *cli,
					      TALLOC_CTX *mem_ctx,
					      struct policy_handle *user_handle);
NTSTATUS rpccli_samr_GetUserPwInfo(struct rpc_pipe_client *cli,
				   TALLOC_CTX *mem_ctx,
				   struct policy_handle *user_handle,
				   struct samr_PwInfo *info);
NTSTATUS rpccli_samr_RemoveMemberFromForeignDomain(struct rpc_pipe_client *cli,
						   TALLOC_CTX *mem_ctx,
						   struct policy_handle *domain_handle,
						   struct dom_sid2 *sid);
NTSTATUS rpccli_samr_QueryDomainInfo2(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      struct policy_handle *domain_handle,
				      uint16_t level,
				      union samr_DomainInfo **info);
NTSTATUS rpccli_samr_QueryUserInfo2(struct rpc_pipe_client *cli,
				    TALLOC_CTX *mem_ctx,
				    struct policy_handle *user_handle,
				    uint16_t level,
				    union samr_UserInfo *info);
NTSTATUS rpccli_samr_QueryDisplayInfo2(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *domain_handle,
				       uint16_t level,
				       uint32_t start_idx,
				       uint32_t max_entries,
				       uint32_t buf_size,
				       uint32_t *total_size,
				       uint32_t *returned_size,
				       union samr_DispInfo *info);
NTSTATUS rpccli_samr_GetDisplayEnumerationIndex2(struct rpc_pipe_client *cli,
						 TALLOC_CTX *mem_ctx,
						 struct policy_handle *domain_handle,
						 uint16_t level,
						 struct lsa_String name,
						 uint32_t *idx);
NTSTATUS rpccli_samr_CreateUser2(struct rpc_pipe_client *cli,
				 TALLOC_CTX *mem_ctx,
				 struct policy_handle *domain_handle,
				 struct lsa_String *account_name,
				 uint32_t acct_flags,
				 uint32_t access_mask,
				 struct policy_handle *user_handle,
				 uint32_t *access_granted,
				 uint32_t *rid);
NTSTATUS rpccli_samr_QueryDisplayInfo3(struct rpc_pipe_client *cli,
				       TALLOC_CTX *mem_ctx,
				       struct policy_handle *domain_handle,
				       uint16_t level,
				       uint32_t start_idx,
				       uint32_t max_entries,
				       uint32_t buf_size,
				       uint32_t *total_size,
				       uint32_t *returned_size,
				       union samr_DispInfo *info);
NTSTATUS rpccli_samr_AddMultipleMembersToAlias(struct rpc_pipe_client *cli,
					       TALLOC_CTX *mem_ctx,
					       struct policy_handle *alias_handle,
					       struct lsa_SidArray *sids);
NTSTATUS rpccli_samr_RemoveMultipleMembersFromAlias(struct rpc_pipe_client *cli,
						    TALLOC_CTX *mem_ctx,
						    struct policy_handle *alias_handle,
						    struct lsa_SidArray *sids);
NTSTATUS rpccli_samr_OemChangePasswordUser2(struct rpc_pipe_client *cli,
					    TALLOC_CTX *mem_ctx,
					    struct lsa_AsciiString *server,
					    struct lsa_AsciiString *account,
					    struct samr_CryptPassword *password,
					    struct samr_Password *hash);
NTSTATUS rpccli_samr_ChangePasswordUser2(struct rpc_pipe_client *cli,
					 TALLOC_CTX *mem_ctx,
					 struct lsa_String *server,
					 struct lsa_String *account,
					 struct samr_CryptPassword *nt_password,
					 struct samr_Password *nt_verifier,
					 uint8_t lm_change,
					 struct samr_CryptPassword *lm_password,
					 struct samr_Password *lm_verifier);
NTSTATUS rpccli_samr_GetDomPwInfo(struct rpc_pipe_client *cli,
				  TALLOC_CTX *mem_ctx,
				  struct lsa_String *domain_name,
				  struct samr_PwInfo *info);
NTSTATUS rpccli_samr_Connect2(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      const char *system_name,
			      uint32_t access_mask,
			      struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_SetUserInfo2(struct rpc_pipe_client *cli,
				  TALLOC_CTX *mem_ctx,
				  struct policy_handle *user_handle,
				  uint16_t level,
				  union samr_UserInfo *info);
NTSTATUS rpccli_samr_SetBootKeyInformation(struct rpc_pipe_client *cli,
					   TALLOC_CTX *mem_ctx,
					   struct policy_handle *connect_handle,
					   uint32_t unknown1,
					   uint32_t unknown2,
					   uint32_t unknown3);
NTSTATUS rpccli_samr_GetBootKeyInformation(struct rpc_pipe_client *cli,
					   TALLOC_CTX *mem_ctx,
					   struct policy_handle *domain_handle,
					   uint32_t *unknown);
NTSTATUS rpccli_samr_Connect3(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      const char *system_name,
			      uint32_t unknown,
			      uint32_t access_mask,
			      struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_Connect4(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      const char *system_name,
			      enum samr_ConnectVersion client_version,
			      uint32_t access_mask,
			      struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_ChangePasswordUser3(struct rpc_pipe_client *cli,
					 TALLOC_CTX *mem_ctx,
					 struct lsa_String *server,
					 struct lsa_String *account,
					 struct samr_CryptPassword *nt_password,
					 struct samr_Password *nt_verifier,
					 uint8_t lm_change,
					 struct samr_CryptPassword *lm_password,
					 struct samr_Password *lm_verifier,
					 struct samr_CryptPassword *password3,
					 struct samr_DomInfo1 **dominfo,
					 struct samr_ChangeReject **reject);
NTSTATUS rpccli_samr_Connect5(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      const char *system_name,
			      uint32_t access_mask,
			      uint32_t level_in,
			      union samr_ConnectInfo *info_in,
			      uint32_t *level_out,
			      union samr_ConnectInfo *info_out,
			      struct policy_handle *connect_handle);
NTSTATUS rpccli_samr_RidToSid(struct rpc_pipe_client *cli,
			      TALLOC_CTX *mem_ctx,
			      struct policy_handle *domain_handle,
			      uint32_t rid,
			      struct dom_sid2 *sid);
NTSTATUS rpccli_samr_SetDsrmPassword(struct rpc_pipe_client *cli,
				     TALLOC_CTX *mem_ctx,
				     struct lsa_String *name,
				     uint32_t unknown,
				     struct samr_Password *hash);
NTSTATUS rpccli_samr_ValidatePassword(struct rpc_pipe_client *cli,
				      TALLOC_CTX *mem_ctx,
				      enum samr_ValidatePasswordLevel level,
				      union samr_ValidatePasswordReq req,
				      union samr_ValidatePasswordRep *rep);
#endif /* __CLI_SAMR__ */
